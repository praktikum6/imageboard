package org.svzh.praktikum.imageboard.services.generic;

import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.ThreadInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ThreadOutDTO;

import javax.annotation.PostConstruct;

public class ThreadServiceTests extends AbstractServiceTests<ThreadEntity, ThreadInDTO, ThreadOutDTO, ThreadRepository, ThreadService> {

    @PostConstruct
    private void init() {
        IN_DTO = ThreadInDTO.builder()
                .boardId(dataInitializer.validBoard.getId())
                .build();
    }
}
