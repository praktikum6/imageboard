package org.svzh.praktikum.imageboard.services.generic;

import org.junit.Assert;
import org.junit.Test;
import org.svzh.praktikum.imageboard.db.ent.BanEntity;
import org.svzh.praktikum.imageboard.db.repo.BanRepository;
import org.svzh.praktikum.imageboard.utils.DateTimeUtils;
import org.svzh.praktikum.imageboard.utils.dto.in.BanInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BanOutDTO;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class BanServiceTests extends AbstractServiceTests<BanEntity, BanInDTO, BanOutDTO, BanRepository, BanService> {

    @PostConstruct
    private void initTestData() {
        IN_DTO = BanInDTO.builder()
                .active(true)
                .comment(faker.lorem().sentence(5))
                .ip(dataInitializer.validBan.getIp())
                .until(DateTimeUtils.getRandomLocalDateTime())
                .build();
    }

    @Test
    public void getByIpTest() {
        String ip = dataInitializer.validBan.getIp();
        List<BanEntity> ents = service.getByIp(ip);
        Assert.assertFalse("Can't get bans by ip " + ip, ents.isEmpty());
    }
}
