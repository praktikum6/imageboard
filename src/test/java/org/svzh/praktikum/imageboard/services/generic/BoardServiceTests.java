package org.svzh.praktikum.imageboard.services.generic;

import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.repo.BoardRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.BoardInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BoardOutDTO;

import javax.annotation.PostConstruct;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class BoardServiceTests extends AbstractServiceTests<BoardEntity, BoardInDTO, BoardOutDTO, BoardRepository, BoardService> {

    @PostConstruct
    private void init() {
        IN_DTO = BoardInDTO.builder()
                .code(faker.lorem().characters(1))
                .name(faker.lorem().word())
                .description(faker.lorem().sentence(5))
                .build();
    }
}
