package org.svzh.praktikum.imageboard.services.generic;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.svzh.praktikum.imageboard.dev.DataInitializer;
import org.svzh.praktikum.imageboard.exceptions.EntityNotFoundException;

import javax.annotation.PostConstruct;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@Slf4j
public abstract class AbstractServiceTests<E, I, O,
        R extends JpaRepository<E, Integer>,
        S extends AbstractService<E, I, O, R>> {

    protected I IN_DTO;
    protected S service;

    @Autowired
    protected DataInitializer dataInitializer;

    @PostConstruct
    private void initData() {
        dataInitializer.initData();
    }

    @Autowired
    private void setService(S service) {
        this.service = service;
    }

    @Test
    public void testCreate() {
        E entity = service.createOrUpdate(IN_DTO);
        Assert.assertNotNull("Can't create new entity!", entity);
    }

    @Test
    public void testConvertInDTOToEntity() {
        E entity = service.inDTOToEntity(IN_DTO);
        Assert.assertNotNull("Can't convert inDTO to entity!", entity);
    }

    @Test
    public void testConvertEntityToOutDTO() {
        Integer id = faker.random().nextInt(1, LEGAL_MAX_ID);
        E entity = service.getById(id);
        O dto = service.entityToOutDTO(entity);
        Assert.assertNotNull("Can't convert entity to OutDTO!", dto);
    }

    @Test
    public void getByIdTest() {
        Integer id = faker.random().nextInt(1, LEGAL_MAX_ID);
        E ent = service.getById(id);
        Assert.assertNotNull("Can't get entity by id " + LEGAL_MAX_ID, ent);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_thenEntityNotFoundException() {
        service.getById(ILLEGAL_MAX_ID);
    }
}
