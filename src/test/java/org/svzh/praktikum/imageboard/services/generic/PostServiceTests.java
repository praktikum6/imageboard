package org.svzh.praktikum.imageboard.services.generic;

import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.repo.PostRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.PostInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.PostOutDTO;

import javax.annotation.PostConstruct;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class PostServiceTests extends AbstractServiceTests<PostEntity, PostInDTO, PostOutDTO, PostRepository, PostService> {

    @PostConstruct
    private void init() {
        IN_DTO = PostInDTO.builder()
                .threadId(dataInitializer.validThread.getId())
                .ip("0.0.0.0")
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
    }
}
