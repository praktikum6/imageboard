package org.svzh.praktikum.imageboard.dev;

import com.github.javafaker.Faker;
import com.google.common.io.Resources;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.svzh.praktikum.imageboard.db.ent.*;
import org.svzh.praktikum.imageboard.services.generic.*;
import org.svzh.praktikum.imageboard.utils.DateTimeUtils;
import org.svzh.praktikum.imageboard.utils.dto.in.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
@SuppressWarnings("UnstableApiUsage")
public class DataInitializer {

    private final BanService banService;
    private final BoardService boardService;
    private final PostService postService;
    private final ThreadService threadService;
    private final ImageService imageService;

    public static final Faker faker = new Faker();

    public BoardEntity validBoard;
    public ThreadEntity validThread;
    public PostEntity validPost;
    public BanEntity validBan;
    public ImageEntity validImage;

    public static final Integer LEGAL_MAX_ID = 10;
    public static final Integer ILLEGAL_MAX_ID = LEGAL_MAX_ID * 2;

    public void initData() {
        initializeBoards();
        initializeThreads();
        initializePosts();
        initializeBans();
        initializeImages();
    }

    private void initializeBans() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            BanInDTO dto = BanInDTO.builder()
                    .active(i == LEGAL_MAX_ID - 1 || faker.random().nextBoolean()) // last one is always active
                    .comment(faker.lorem().sentence(5))
                    .ip(faker.internet().ipV4Address())
                    .until(i == LEGAL_MAX_ID - 1 ? LocalDateTime.now().plusDays(100) : DateTimeUtils.getRandomLocalDateTime())
                    .build();
            validBan = banService.createOrUpdate(dto);
        }
    }

    private void initializeBoards() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            BoardInDTO dto = BoardInDTO.builder()
                    .code(faker.lorem().characters(1))
                    .name(faker.lorem().word())
                    .description(faker.lorem().sentence(5))
                    .build();
            validBoard = boardService.createOrUpdate(dto);
        }
    }

    private void initializePosts() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            PostInDTO dto = PostInDTO.builder()
                    .ip(faker.internet().ipV4Address())
                    .author(faker.internet().emailAddress())
                    .content(faker.lorem().paragraph())
                    .threadId(faker.random().nextInt(1, LEGAL_MAX_ID))
                    .build();
            validPost = postService.createOrUpdate(dto);
        }
    }

    private void initializeThreads() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            ThreadInDTO dto = ThreadInDTO.builder()
                    .boardId(faker.random().nextInt(1, LEGAL_MAX_ID))
                    .build();
            validThread = threadService.createOrUpdate(dto);
        }
    }

    private void initializeImages() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            try {
                URL resource = Resources.getResource("anonymous.jpg");

                MultipartFile multipartFile =
                        new MockMultipartFile(faker.file().fileName(),
                                new FileInputStream(resource.getFile()));

                ImageInDTO dto = ImageInDTO.builder()
                        .ip(faker.internet().ipV4Address())
                        .file(multipartFile)
                        .build();

                validImage = imageService.createOrUpdate(dto);
            } catch(IOException e) {
                throw new RuntimeException("Can't initialize test images!");
            }
        }
    }
}
