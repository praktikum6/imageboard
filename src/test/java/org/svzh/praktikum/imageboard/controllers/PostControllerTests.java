package org.svzh.praktikum.imageboard.controllers;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.repo.PostRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.PostInDTO;

import javax.annotation.PostConstruct;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.ILLEGAL_MAX_ID;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class PostControllerTests extends AbstractControllerTests<PostEntity, PostInDTO, PostRepository> {

    private PostInDTO BANNED_IN_DTO;
    private PostInDTO ILLEGAL_DTO_1;
    private PostInDTO ILLEGAL_DTO_2;
    private PostInDTO ILLEGAL_DTO_3;
    private static final String NOT_BANNED_IP = "0.0.0.0";

    @PostConstruct
    private void init() {
        IN_DTO = PostInDTO.builder()
                .threadId(dataInitializer.validThread.getId())
                .ip(NOT_BANNED_IP)
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
        BANNED_IN_DTO = PostInDTO.builder()
                .threadId(dataInitializer.validThread.getId())
                .ip(dataInitializer.validBan.getIp())
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
        ILLEGAL_DTO_1 = PostInDTO.builder()
                .threadId(null) // thread should be specified
                .ip(NOT_BANNED_IP)
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
        ILLEGAL_DTO_2 = PostInDTO.builder()
                .threadId(ILLEGAL_MAX_ID) // there is no such thread
                .ip(NOT_BANNED_IP)
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
        ILLEGAL_DTO_3 = PostInDTO.builder()
                .threadId(dataInitializer.validThread.getId())
                .ip(null) // ip can't be null
                .author(faker.internet().emailAddress())
                .content(faker.lorem().paragraph())
                .build();
        REQUEST_MAPPING = "/post";
    }

    @Test
    public void testCreateWhenBanned_ThenException() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(BANNED_IN_DTO);
        final String ip = BANNED_IN_DTO.getIp();
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Ip " + ip + " is banned from posting!")));
    }

    @Test
    public void testCreateWhenThreadIsNull_ThenException() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(ILLEGAL_DTO_1);
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Thread ID is not specified!")));
    }

    @Test
    public void testCreateWhenThreadIsIllegal_ThenException() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(ILLEGAL_DTO_2);
        final Integer threadId = ILLEGAL_DTO_2.getThreadId();
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Thread not found by ID: " + threadId)));
    }

    @Test
    public void testCreateWhenAuthorIpIsNull_ThenException() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(ILLEGAL_DTO_3);
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Author IP is not specified!")));
    }
}
