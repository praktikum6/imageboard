package org.svzh.praktikum.imageboard.controllers;

import org.svzh.praktikum.imageboard.db.ent.BanEntity;
import org.svzh.praktikum.imageboard.db.repo.BanRepository;
import org.svzh.praktikum.imageboard.utils.DateTimeUtils;
import org.svzh.praktikum.imageboard.utils.dto.in.BanInDTO;

import javax.annotation.PostConstruct;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class BanControllerTests extends AbstractControllerTests<BanEntity, BanInDTO, BanRepository> {

    @PostConstruct
    private void init() {
        IN_DTO = BanInDTO.builder()
                .active(faker.random().nextBoolean())
                .comment(faker.lorem().sentence(5))
                .ip(faker.internet().ipV4Address())
                .until(DateTimeUtils.getRandomLocalDateTime())
                .build();
        REQUEST_MAPPING = "/ban";
    }
}
