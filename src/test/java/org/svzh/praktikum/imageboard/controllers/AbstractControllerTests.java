package org.svzh.praktikum.imageboard.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.svzh.praktikum.imageboard.db.ent.AbstractEntity;
import org.svzh.praktikum.imageboard.dev.DataInitializer;
import org.svzh.praktikum.imageboard.utils.dto.in.AbstractInDTO;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.ILLEGAL_MAX_ID;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.LEGAL_MAX_ID;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@Slf4j
public abstract class AbstractControllerTests<E extends AbstractEntity, I extends AbstractInDTO, R extends JpaRepository<E, Integer>> {

    protected MockMvc mockMvc;
    protected I IN_DTO;
    protected String REQUEST_MAPPING;
    protected String VALID_PAGE_VALUE = String.valueOf(0);
    protected String VALID_SIZE_VALUE = String.valueOf(10);
    protected String ILLEGAL_PAGE_VALUE = String.valueOf(-1);
    protected String ILLEGAL_SIZE_VALUE = String.valueOf(-1);

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected DataInitializer dataInitializer;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected R repository;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @PostConstruct
    private void initData() {
        this.mockMvc =
                MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                        .apply(documentationConfiguration(this.restDocumentation))
                        .build();
        dataInitializer.initData();
    }

    // TODO: 26.08.2021 проверять не просто isOk, а какие-то данные в выдаче

    @Test
    public void testGetAll() throws Exception {
        final String URI = getRequestMappingAll();
        mockMvc.perform(get(URI).contentType(MediaType.APPLICATION_JSON))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllPaged() throws Exception {
        final String URI = getRequestMappingAll();
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", VALID_PAGE_VALUE)
                        .param("size", VALID_SIZE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetById() throws Exception {
        final String URI = getRequestMappingId();
        mockMvc.perform(get(URI, LEGAL_MAX_ID).contentType(MediaType.APPLICATION_JSON))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreatePost() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(IN_DTO);
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreatePut() throws Exception {
        final String URI = getRequestMapping();
        final String content = objectMapper.writeValueAsString(IN_DTO);
        mockMvc.perform(put(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetByIdWithIllegalArgumentType_thenBadRequest() throws Exception {
        final String ERROR_MSG = "id should be of type java.lang.Integer";
        final String URI = getRequestMappingId();
        mockMvc.perform(get(URI, "blah-de-blah").contentType(MediaType.APPLICATION_JSON))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains(ERROR_MSG)));
    }

    @Test
    public void whenInternalException_thenBadRequest() throws Exception {
        final String URI = getRequestMappingId();
        final Integer idToDelete = ILLEGAL_MAX_ID;
        mockMvc.perform(delete(URI, idToDelete).contentType(MediaType.APPLICATION_JSON))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains("There is no " + getEntityClass().getSimpleName() + " with id " + idToDelete)));
    }

    @Test
    public void testGetAllPagedParamNotSpecified_ThenBadRequest() throws Exception {
        final String ERROR_MSG = "Size and page should be both specified or null.";
        final String URI = getRequestMappingAll();
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("size", VALID_SIZE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains(ERROR_MSG)));
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", VALID_PAGE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains(ERROR_MSG)));
    }

    @Test
    public void testGetAllPagedOutOfScope_ThenBadRequest() throws Exception {
        final String ERROR_MSG = "Size should be above zero and page should not be lesser then zero.";
        final String URI = getRequestMappingAll();
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", ILLEGAL_PAGE_VALUE)
                        .param("size", ILLEGAL_SIZE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains(ERROR_MSG)));
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", VALID_PAGE_VALUE)
                        .param("size", ILLEGAL_SIZE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains(ERROR_MSG)));
        mockMvc.perform(get(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", ILLEGAL_PAGE_VALUE)
                        .param("size", VALID_SIZE_VALUE)
                ).andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors",
                        Matchers.contains(ERROR_MSG)));
    }

    @Test
    public void testDelete() throws Exception {
        final String URI = getRequestMappingId();
        Assert.assertTrue("There was not such entity to remove!", repository.existsById(LEGAL_MAX_ID));
        mockMvc.perform(delete(URI, LEGAL_MAX_ID).contentType(MediaType.APPLICATION_JSON))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isOk());
        Assert.assertFalse("The entity was not removed!", repository.existsById(LEGAL_MAX_ID));
    }

    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        return (Class<E>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected String getRequestMapping() {
        return this.REQUEST_MAPPING;
    }

    protected String getRequestMappingAll() {
        return this.REQUEST_MAPPING + "/all";
    }

    protected String getRequestMappingId() {
        return this.REQUEST_MAPPING + "/{id}";
    }
}
