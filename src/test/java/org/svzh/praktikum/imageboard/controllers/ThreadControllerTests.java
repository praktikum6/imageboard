package org.svzh.praktikum.imageboard.controllers;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.ThreadInDTO;

import javax.annotation.PostConstruct;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.svzh.praktikum.imageboard.dev.DataInitializer.ILLEGAL_MAX_ID;

public class ThreadControllerTests extends AbstractControllerTests<ThreadEntity, ThreadInDTO, ThreadRepository> {

    private ThreadInDTO ILLEGAL_DTO_1;
    private ThreadInDTO ILLEGAL_DTO_2;

    @PostConstruct
    private void init() {
        IN_DTO = ThreadInDTO.builder()
                .boardId(dataInitializer.validBoard.getId())
                .build();
        ILLEGAL_DTO_1 = ThreadInDTO.builder()
                .build();
        ILLEGAL_DTO_2 = ThreadInDTO.builder()
                .boardId(ILLEGAL_MAX_ID)
                .build();
        REQUEST_MAPPING = "/thread";
    }

    @Test
    public void testCreateWhenBoardIsNull_ThenException() throws Exception {
        final String URI = getRequestMapping();
        String content = objectMapper.writeValueAsString(ILLEGAL_DTO_1);
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Board ID is not specified!")));
    }

    @Test
    public void testCreateWhenBoardIsIllegal_ThenException() throws Exception {
        final String URI = getRequestMapping();
        String content = objectMapper.writeValueAsString(ILLEGAL_DTO_2);
        Integer boardId = ILLEGAL_DTO_2.getBoardId();
        mockMvc.perform(post(URI).contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(document(URI.replace("/", "\\")))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.contains("Board not found by ID: " + boardId)));
    }
}
