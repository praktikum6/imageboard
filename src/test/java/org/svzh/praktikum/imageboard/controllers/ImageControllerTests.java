package org.svzh.praktikum.imageboard.controllers;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.svzh.praktikum.imageboard.db.ent.ImageEntity;
import org.svzh.praktikum.imageboard.db.repo.ImageRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.ImageInDTO;
import com.google.common.io.Resources;

import javax.annotation.PostConstruct;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

@SuppressWarnings("UnstableApiUsage")
@Slf4j
public class ImageControllerTests extends AbstractControllerTests<ImageEntity, ImageInDTO, ImageRepository> {

    @PostConstruct
    private void init() {
        try {
            URL resource = Resources.getResource("anonymous.jpg");

            MultipartFile multipartFile =
                    new MockMultipartFile(faker.file().fileName(),
                            new FileInputStream(resource.getFile()));

            IN_DTO = ImageInDTO.builder()
                    .ip(faker.internet().ipV4Address())
                    .file(multipartFile)
                    .build();
        } catch(IOException e) {
            throw new RuntimeException("Can't initialize test images!");
        }
        REQUEST_MAPPING = "/image";
    }

    @Override
    public void testGetAll() throws Exception {
        Assert.assertTrue(true);
        // Not applicable
    }

    @Override
    public void testGetAllPaged() throws Exception {
        Assert.assertTrue(true);
        // Not applicable
    }

    @Override
    public void testGetById() throws Exception {
        Assert.assertTrue(true);
        // Not applicable
    }

    @Override
    public void testGetAllPagedParamNotSpecified_ThenBadRequest() throws Exception {
        Assert.assertTrue(true);
        // Not applicable
    }

    @Override
    public void testGetAllPagedOutOfScope_ThenBadRequest() throws Exception {
        Assert.assertTrue(true);
        // Not applicable
    }
}
