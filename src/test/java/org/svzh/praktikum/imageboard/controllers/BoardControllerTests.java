package org.svzh.praktikum.imageboard.controllers;

import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.repo.BoardRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.BoardInDTO;

import javax.annotation.PostConstruct;

import static org.svzh.praktikum.imageboard.dev.DataInitializer.faker;

public class BoardControllerTests extends AbstractControllerTests<BoardEntity, BoardInDTO, BoardRepository> {

    @PostConstruct
    private void init() {
        IN_DTO = BoardInDTO.builder()
                .code(faker.lorem().characters(1))
                .name(faker.lorem().word())
                .description(faker.lorem().sentence(5))
                .build();
        REQUEST_MAPPING = "/board";
    }
}
