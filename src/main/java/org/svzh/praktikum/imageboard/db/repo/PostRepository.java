package org.svzh.praktikum.imageboard.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Integer> {
    List<PostEntity> findByThreadEquals(ThreadEntity thread);

    List<PostEntity> findByCreatedAfter(LocalDateTime date);
}
