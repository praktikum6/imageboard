package org.svzh.praktikum.imageboard.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.svzh.praktikum.imageboard.db.ent.BanEntity;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BanRepository extends JpaRepository<BanEntity, Integer> {
    List<BanEntity> findBanEntitiesByIpEquals(String ip);

    boolean existsByIpEquals(String ip);

    boolean existsByIpEqualsAndUntilAfter(String ip, LocalDateTime until);
}
