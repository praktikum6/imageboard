package org.svzh.praktikum.imageboard.db.ent;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name = "POST")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class PostEntity extends AbstractEntity {

    private String author;

    @Column(length = 16000)
    private String content;

    private String ip;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ThreadEntity thread;

    @OneToOne
    private ImageEntity image;
}
