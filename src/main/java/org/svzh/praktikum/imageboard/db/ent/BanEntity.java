package org.svzh.praktikum.imageboard.db.ent;

import lombok.*;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity(name = "BAN")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class BanEntity extends AbstractEntity {

    private String ip;

    private Boolean active;

    private String comment;

    private LocalDateTime until;
}
