package org.svzh.praktikum.imageboard.db.ent;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "BOARD")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class BoardEntity extends AbstractEntity {

    private String code;

    private String name;

    private String description;

    @OneToMany(mappedBy = "board")
    @JsonIgnore
    @ToString.Exclude
    private List<ThreadEntity> threads;
}
