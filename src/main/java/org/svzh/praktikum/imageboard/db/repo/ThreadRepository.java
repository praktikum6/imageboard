package org.svzh.praktikum.imageboard.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;

import java.util.List;

@Repository
public interface ThreadRepository extends JpaRepository<ThreadEntity, Integer> {
    List<ThreadEntity> findByBoardEquals(BoardEntity board);
}
