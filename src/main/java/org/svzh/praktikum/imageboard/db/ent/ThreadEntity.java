package org.svzh.praktikum.imageboard.db.ent;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "THREAD")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class ThreadEntity extends AbstractEntity {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private BoardEntity board;

    @OneToMany(mappedBy = "thread")
    @JsonIgnore
    @ToString.Exclude
    private List<PostEntity> posts;
}
