package org.svzh.praktikum.imageboard.db.ent;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity(name = "IMAGE")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class ImageEntity extends AbstractEntity {

    private String ip;

    @Lob
    private byte[] content;

    private String originalFileName;
}
