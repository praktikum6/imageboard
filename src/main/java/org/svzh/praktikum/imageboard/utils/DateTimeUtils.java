package org.svzh.praktikum.imageboard.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.svzh.praktikum.imageboard.annotations.GeneratedOrConfigOrUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@GeneratedOrConfigOrUtils
public class DateTimeUtils {

    private static final String SIMPLE_DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";

    public static String localDateTimeToString(LocalDateTime dateTime) {
        return Objects.isNull(dateTime) ? "" : (DateTimeFormatter.ofPattern(SIMPLE_DATE_TIME_FORMAT)).format(dateTime);
    }

    public static LocalDateTime getRandomLocalDateTime() {
        LocalDateTime now = LocalDateTime.now();
        int year = 60 * 60 * 24 * 365;
        return now.plusSeconds((long) (Math.random() * (4 * year) - (2 * year)));
    }
}
