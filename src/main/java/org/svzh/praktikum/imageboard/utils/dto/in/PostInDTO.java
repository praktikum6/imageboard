package org.svzh.praktikum.imageboard.utils.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class PostInDTO extends AbstractInDTO {
    private String author;
    private String content;
    private String ip;
    private Integer threadId;
    private Integer imageId;
}
