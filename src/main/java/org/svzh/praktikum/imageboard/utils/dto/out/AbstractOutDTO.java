package org.svzh.praktikum.imageboard.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;
import org.svzh.praktikum.imageboard.utils.dto.AbstractDTO;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractOutDTO implements AbstractDTO {
    @NonNull
    private Integer id;
    private LocalDateTime created;
    private LocalDateTime modified;
}
