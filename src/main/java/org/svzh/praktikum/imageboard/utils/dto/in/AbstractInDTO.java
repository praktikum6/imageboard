package org.svzh.praktikum.imageboard.utils.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.svzh.praktikum.imageboard.utils.dto.AbstractDTO;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractInDTO implements AbstractDTO {
    private Integer id;
}
