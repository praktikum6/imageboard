package org.svzh.praktikum.imageboard.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ImageOutDTO extends AbstractOutDTO {
    private String ip;
    private byte[] content;
    private String originalFileName;
}
