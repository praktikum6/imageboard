package org.svzh.praktikum.imageboard.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class PostOutDTO extends AbstractOutDTO {
    private String author;
    private String content;
    private String ip;
    private Integer threadId;
    private Integer imageId;
    private String imageName;
}
