package org.svzh.praktikum.imageboard.utils.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class BanInDTO extends AbstractInDTO {
    private String ip;
    private Boolean active;
    private String comment;
    private LocalDateTime until;
}
