package org.svzh.praktikum.imageboard.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
public class BanOutDTO extends AbstractOutDTO {
    private String ip;
    private Boolean active;
    private String comment;
    private LocalDateTime until;
}
