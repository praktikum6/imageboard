package org.svzh.praktikum.imageboard.utils.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class ThreadInDTO extends AbstractInDTO {
    private Integer boardId;
}
