package org.svzh.praktikum.imageboard.utils.dto.in;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class ImageInDTO extends AbstractInDTO {
    private String ip;
    @JsonIgnore
    private MultipartFile file;
}
