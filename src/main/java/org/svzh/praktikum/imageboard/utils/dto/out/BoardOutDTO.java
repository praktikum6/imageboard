package org.svzh.praktikum.imageboard.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class BoardOutDTO extends AbstractOutDTO {
    private String code;
    private String name;
    private String description;
}
