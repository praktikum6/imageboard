package org.svzh.praktikum.imageboard.exceptions;

public abstract class ImageBoardAbstractException extends RuntimeException {
    protected ImageBoardAbstractException(String msg) {
        super(msg);
    }
}
