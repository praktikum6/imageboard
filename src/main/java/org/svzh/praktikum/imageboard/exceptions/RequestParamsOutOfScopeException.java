package org.svzh.praktikum.imageboard.exceptions;

public class RequestParamsOutOfScopeException extends ImageBoardAbstractException {
    public RequestParamsOutOfScopeException(String msg) {
        super(msg);
    }
}
