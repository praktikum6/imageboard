package org.svzh.praktikum.imageboard.exceptions;

public class EntityNotFoundException extends ImageBoardAbstractException {
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
