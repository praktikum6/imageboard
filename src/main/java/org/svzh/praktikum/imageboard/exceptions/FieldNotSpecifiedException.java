package org.svzh.praktikum.imageboard.exceptions;

public class FieldNotSpecifiedException extends ImageBoardAbstractException {
    public FieldNotSpecifiedException(String msg) {
        super(msg);
    }
}
