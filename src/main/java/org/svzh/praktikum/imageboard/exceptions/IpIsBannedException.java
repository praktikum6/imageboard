package org.svzh.praktikum.imageboard.exceptions;

public class IpIsBannedException extends ImageBoardAbstractException {
    public IpIsBannedException(String msg) {
        super(msg);
    }
}
