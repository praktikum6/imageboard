package org.svzh.praktikum.imageboard.exceptions;

public class FileNotFoundException extends ImageBoardAbstractException {
    public FileNotFoundException(String msg) {
        super(msg);
    }
}
