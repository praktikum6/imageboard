package org.svzh.praktikum.imageboard.exceptions;

public class RequestParamsNotProvidedException extends ImageBoardAbstractException {
    public RequestParamsNotProvidedException(String msg) {
        super(msg);
    }
}
