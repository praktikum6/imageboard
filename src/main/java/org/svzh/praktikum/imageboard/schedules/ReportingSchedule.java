package org.svzh.praktikum.imageboard.schedules;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.svzh.praktikum.imageboard.annotations.GeneratedOrConfigOrUtils;
import org.svzh.praktikum.imageboard.services.util.ReportingService;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Slf4j
@Component
@GeneratedOrConfigOrUtils
public class ReportingSchedule {

    @Value("${spring.application.reporting.enabled}")
    private Boolean cronEnabled;

    private final ReportingService service;

    @Autowired
    public ReportingSchedule(ReportingService service) {
        this.service = service;
    }

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    @Scheduled(cron = "${spring.application.reporting.cron}")
    public void report() {
        if (Optional.ofNullable(cronEnabled).orElse(false)) {
            service.report();
        }
    }
}
