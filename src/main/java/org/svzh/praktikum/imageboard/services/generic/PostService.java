package org.svzh.praktikum.imageboard.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.ImageEntity;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.ImageRepository;
import org.svzh.praktikum.imageboard.db.repo.PostRepository;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.exceptions.EntityNotFoundException;
import org.svzh.praktikum.imageboard.exceptions.FieldNotSpecifiedException;
import org.svzh.praktikum.imageboard.utils.dto.in.PostInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.PostOutDTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.HOURS;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostService extends AbstractService<PostEntity, PostInDTO, PostOutDTO, PostRepository> {

    private final ThreadRepository threadRepository;
    private final ImageRepository imageRepository;

    public List<PostEntity> getPostsForTheLastHour() {
        LocalDateTime hourAgo = LocalDateTime.now().minus(1, HOURS);
        return repository.findByCreatedAfter(hourAgo);
    }

    @Override
    public PostEntity createOrUpdate(PostInDTO dto) {
        Integer threadId = Optional.ofNullable(dto.getThreadId()).orElseThrow(() -> new FieldNotSpecifiedException("Thread ID is not specified!"));
        ThreadEntity thread = threadRepository.findById(threadId).orElseThrow(() -> new EntityNotFoundException("Thread not found by ID: " + threadId));
        Integer imageId = Optional.ofNullable(dto.getImageId()).orElse(null);
        ImageEntity image = imageId == null ? null : imageRepository.findById(imageId).orElse(null);
        PostEntity entity = this.inDTOToEntity(dto);
        entity.setThread(thread);
        entity.setImage(image);
        return repository.save(entity);
    }

    @Override
    public PostOutDTO entityToOutDTO(PostEntity entity) {
        PostOutDTO dto = super.entityToOutDTO(entity);
        dto.setThreadId(entity.getThread().getId());
        Optional.ofNullable(entity.getImage())
                .ifPresent(image -> {
                    dto.setImageId(image.getId());
                    dto.setImageName(image.getOriginalFileName());
                });
        return dto;
    }
}
