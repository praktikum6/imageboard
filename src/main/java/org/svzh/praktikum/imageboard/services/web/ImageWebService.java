package org.svzh.praktikum.imageboard.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.ImageEntity;
import org.svzh.praktikum.imageboard.db.repo.ImageRepository;
import org.svzh.praktikum.imageboard.services.generic.ImageService;
import org.svzh.praktikum.imageboard.utils.dto.in.ImageInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ImageOutDTO;

@Service
@RequiredArgsConstructor
@Slf4j
public class ImageWebService extends AbstractWebService<ImageEntity, ImageInDTO, ImageOutDTO, ImageRepository, ImageService> {

    public byte[] getImageContentById(Integer id) {
        return service.getImageContentById(id);
    }

    @Override
    public ImageOutDTO createOrUpdate(ImageInDTO dto) {
        return service.entityToOutDTO(service.createOrUpdate(dto));
    }
}
