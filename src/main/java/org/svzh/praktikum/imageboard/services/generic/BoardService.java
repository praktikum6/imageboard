package org.svzh.praktikum.imageboard.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.repo.BoardRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.BoardInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BoardOutDTO;

@Service
@RequiredArgsConstructor
@Slf4j
public class BoardService extends AbstractService<BoardEntity, BoardInDTO, BoardOutDTO, BoardRepository> {
}
