package org.svzh.praktikum.imageboard.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.repo.PostRepository;
import org.svzh.praktikum.imageboard.exceptions.FieldNotSpecifiedException;
import org.svzh.praktikum.imageboard.exceptions.IpIsBannedException;
import org.svzh.praktikum.imageboard.services.generic.BanService;
import org.svzh.praktikum.imageboard.services.generic.PostService;
import org.svzh.praktikum.imageboard.utils.dto.in.PostInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.PostOutDTO;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostWebService extends AbstractWebService<PostEntity, PostInDTO, PostOutDTO, PostRepository, PostService> {

    private final BanService banService;

    public void checkIfBanned(PostInDTO dto) {
        String ip = Optional.ofNullable(dto.getIp()).orElseThrow(() -> new FieldNotSpecifiedException("Author IP is not specified!"));
        if (banService.banExistsByIp(ip)) {
            throw new IpIsBannedException("Ip " + ip + " is banned from posting!");
        }
    }

    @Override
    public PostOutDTO createOrUpdate(PostInDTO dto) {
        checkIfBanned(dto);
        return super.createOrUpdate(dto);
    }
}
