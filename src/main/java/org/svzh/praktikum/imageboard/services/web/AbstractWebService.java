package org.svzh.praktikum.imageboard.services.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.svzh.praktikum.imageboard.db.ent.AbstractEntity;
import org.svzh.praktikum.imageboard.exceptions.RequestParamsNotProvidedException;
import org.svzh.praktikum.imageboard.exceptions.RequestParamsOutOfScopeException;
import org.svzh.praktikum.imageboard.services.generic.AbstractService;
import org.svzh.praktikum.imageboard.utils.dto.in.AbstractInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.AbstractOutDTO;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractWebService<E extends AbstractEntity,
        I extends AbstractInDTO,
        O extends AbstractOutDTO,
        R extends JpaRepository<E, Integer>,
        S extends AbstractService<E, I, O, R>> {

    protected S service;

    @Autowired
    private void setService(S service) {
        this.service = service;
    }

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    public List<O> getAll(Integer page, Integer size) {
        if (page != null && size != null) {
            if (page >= 0 && size > 0) {
                return service.getAll(page, size).stream()
                        .map(service::entityToOutDTO)
                        .collect(Collectors.toList());
            } else {
                throw new RequestParamsOutOfScopeException("Size should be above zero and page should not be lesser then zero.");
            }
        } else if (page != null || size != null) {
            throw new RequestParamsNotProvidedException("Size and page should be both specified or null.");
        } else {
            return service.getAll().stream()
                    .map(service::entityToOutDTO)
                    .collect(Collectors.toList());
        }
    }

    public O getById(Integer id) {
        return service.entityToOutDTO(service.getById(id));
    }

    public void deleteById(Integer id) {
        service.deleteById(id);
    }

    public O createOrUpdate(I dto) {
        return service.entityToOutDTO(service.createOrUpdate(dto));
    }
}
