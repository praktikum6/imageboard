package org.svzh.praktikum.imageboard.services.util;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.annotations.GeneratedOrConfigOrUtils;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.services.generic.PostService;
import org.svzh.praktikum.imageboard.utils.DateTimeUtils;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@GeneratedOrConfigOrUtils
public class ReportingService {

    private final EmailService emailService;
    private final PostService postService;

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    public void report() {
        log.info("Scheduled reporting!");
        List<PostEntity> postsFromLastHour = postService.getPostsForTheLastHour();
        int postsCount = postsFromLastHour.size();
        String body = String.format("There was %s posts during last hour!", postsCount);
        String subject = "Scheduled report at " + DateTimeUtils.localDateTimeToString(LocalDateTime.now());
        emailService.sendSimpleMessage("admin@wipe-squad.ru", subject, body);
    }
}
