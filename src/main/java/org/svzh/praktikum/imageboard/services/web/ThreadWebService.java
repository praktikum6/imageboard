package org.svzh.praktikum.imageboard.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.services.generic.ThreadService;
import org.svzh.praktikum.imageboard.utils.dto.in.ThreadInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ThreadOutDTO;

@Service
@RequiredArgsConstructor
@Slf4j
public class ThreadWebService extends AbstractWebService<ThreadEntity, ThreadInDTO, ThreadOutDTO, ThreadRepository, ThreadService> {
}
