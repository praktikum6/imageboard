package org.svzh.praktikum.imageboard.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.BanEntity;
import org.svzh.praktikum.imageboard.db.repo.BanRepository;
import org.svzh.praktikum.imageboard.services.generic.BanService;
import org.svzh.praktikum.imageboard.utils.dto.in.BanInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BanOutDTO;

@Service
@RequiredArgsConstructor
@Slf4j
public class BanWebService extends AbstractWebService<BanEntity, BanInDTO, BanOutDTO, BanRepository, BanService> {
}
