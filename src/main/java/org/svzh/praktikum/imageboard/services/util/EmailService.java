package org.svzh.praktikum.imageboard.services.util;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.annotations.GeneratedOrConfigOrUtils;

import javax.annotation.PostConstruct;

@Service
@Slf4j
@RequiredArgsConstructor
@GeneratedOrConfigOrUtils
public class EmailService {

    private final JavaMailSender emailSender;

    @Value("${spring.mail.from}")
    private String from;

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }
}
