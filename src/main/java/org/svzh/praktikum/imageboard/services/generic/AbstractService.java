package org.svzh.praktikum.imageboard.services.generic;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.svzh.praktikum.imageboard.exceptions.EntityNotFoundException;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Slf4j
public abstract class AbstractService<E, I, O, R extends JpaRepository<E, Integer>> {

    protected ObjectMapper mapper;
    protected R repository;

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    public List<E> getAll() {
        return repository.findAll();
    }

    public Page<E> getAll(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page, size));
    }

    public E getById(Integer id) {
        return repository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(
                        String.format("There is no %s with id %s",
                                getEntityClass().getSimpleName(), id)));
    }

    public E createOrUpdate(I dto) {
        return repository.save(inDTOToEntity(dto));
    }

    public void deleteById(Integer id) {
        repository.delete(getById(id));
    }

    public E inDTOToEntity(I dto) {
        return mapper.convertValue(dto, getEntityClass());
    }

    public O entityToOutDTO(E entity) {
        return mapper.convertValue(entity, getOutDTOClass());
    }

    /**
     * Setters here
     */

    @Autowired
    public final void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public final void setRepository(R repository) {
        this.repository = repository;
    }

    /**
     * Reflection-fu
     */

    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        return (Class<E>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    private Class<O> getOutDTOClass() {
        return (Class<O>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];
    }
}
