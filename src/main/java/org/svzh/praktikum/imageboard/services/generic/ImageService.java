package org.svzh.praktikum.imageboard.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.svzh.praktikum.imageboard.db.ent.ImageEntity;
import org.svzh.praktikum.imageboard.db.repo.ImageRepository;
import org.svzh.praktikum.imageboard.exceptions.FieldNotSpecifiedException;
import org.svzh.praktikum.imageboard.exceptions.FileNotFoundException;
import org.svzh.praktikum.imageboard.utils.dto.in.ImageInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ImageOutDTO;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class ImageService extends AbstractService<ImageEntity, ImageInDTO, ImageOutDTO, ImageRepository> {

    @Override
    public ImageOutDTO entityToOutDTO(ImageEntity entity) {
        return mapper.convertValue(entity, ImageOutDTO.class);
    }

    @Override
    public ImageEntity createOrUpdate(ImageInDTO dto) {
        return repository.save(inDTOToEntity(dto));
    }

    @Override
    public ImageEntity inDTOToEntity(ImageInDTO dto) {
        ImageEntity entity = mapper.convertValue(dto, ImageEntity.class);
        MultipartFile file = dto.getFile();
        if (file != null && !file.isEmpty()) {
            try {
                entity.setContent(file.getBytes());
                entity.setOriginalFileName(file.getOriginalFilename());
            } catch (IOException e) {
                throw new FieldNotSpecifiedException("Can't get file from uploaded schmuck!");
            }
        }
        return entity;
    }

    public byte[] getImageContentById(Integer id) {
        return repository.findById(id)
                .map(ImageEntity::getContent)
                .orElseThrow(() -> new FileNotFoundException("Can't get image with id " + id));
    }
}
