package org.svzh.praktikum.imageboard.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.BanEntity;
import org.svzh.praktikum.imageboard.db.repo.BanRepository;
import org.svzh.praktikum.imageboard.utils.dto.in.BanInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BanOutDTO;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BanService extends AbstractService<BanEntity, BanInDTO, BanOutDTO, BanRepository> {

    public List<BanEntity> getByIp(String ip) {
        return repository.findBanEntitiesByIpEquals(ip);
    }

    public boolean banExistsByIp(String ip) {
        boolean banned = repository.existsByIpEqualsAndUntilAfter(ip, LocalDateTime.now());
        log.info("Banned for ip {}: {}", ip, banned);
        return banned;
    }

}
