package org.svzh.praktikum.imageboard.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.BoardRepository;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.exceptions.EntityNotFoundException;
import org.svzh.praktikum.imageboard.exceptions.FieldNotSpecifiedException;
import org.svzh.praktikum.imageboard.utils.dto.in.ThreadInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ThreadOutDTO;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ThreadService extends AbstractService<ThreadEntity, ThreadInDTO, ThreadOutDTO, ThreadRepository> {

    private final BoardRepository boardRepository;

    @Override
    public ThreadEntity createOrUpdate(ThreadInDTO dto) {
        Integer boardId = Optional.ofNullable(dto.getBoardId()).orElseThrow(() -> new FieldNotSpecifiedException("Board ID is not specified!"));
        BoardEntity board = boardRepository.findById(boardId).orElseThrow(() -> new EntityNotFoundException("Board not found by ID: " + boardId));
        ThreadEntity entity = this.inDTOToEntity(dto);
        entity.setBoard(board);
        return repository.save(entity);
    }

    @Override
    public ThreadOutDTO entityToOutDTO(ThreadEntity entity) {
        ThreadOutDTO dto = super.entityToOutDTO(entity);
        dto.setBoardId(entity.getBoard().getId());
        return dto;
    }
}
