package org.svzh.praktikum.imageboard.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.svzh.praktikum.imageboard.db.ent.PostEntity;
import org.svzh.praktikum.imageboard.db.repo.PostRepository;
import org.svzh.praktikum.imageboard.services.generic.PostService;
import org.svzh.praktikum.imageboard.services.web.PostWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.PostInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.PostOutDTO;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
@Slf4j
public class PostController extends AbstractController<PostEntity, PostInDTO, PostOutDTO, PostRepository, PostService, PostWebService> {
}
