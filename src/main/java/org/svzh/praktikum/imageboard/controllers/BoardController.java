package org.svzh.praktikum.imageboard.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.svzh.praktikum.imageboard.db.ent.BoardEntity;
import org.svzh.praktikum.imageboard.db.repo.BoardRepository;
import org.svzh.praktikum.imageboard.services.generic.BoardService;
import org.svzh.praktikum.imageboard.services.web.BoardWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.BoardInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BoardOutDTO;

@RestController
@RequestMapping("/board")
@RequiredArgsConstructor
@Slf4j
public class BoardController extends AbstractController<BoardEntity, BoardInDTO, BoardOutDTO, BoardRepository, BoardService, BoardWebService> {
}
