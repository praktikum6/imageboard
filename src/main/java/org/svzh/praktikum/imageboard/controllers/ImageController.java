package org.svzh.praktikum.imageboard.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.svzh.praktikum.imageboard.db.ent.ImageEntity;
import org.svzh.praktikum.imageboard.db.repo.ImageRepository;
import org.svzh.praktikum.imageboard.exceptions.FileNotFoundException;
import org.svzh.praktikum.imageboard.services.generic.ImageService;
import org.svzh.praktikum.imageboard.services.web.ImageWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.ImageInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ImageOutDTO;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
@Slf4j
public class ImageController extends AbstractController<ImageEntity, ImageInDTO, ImageOutDTO, ImageRepository, ImageService, ImageWebService> {

    @Override
    @RequestMapping(method = {PUT, POST})
    public ResponseEntity<ImageOutDTO> createOrUpdatePost(@ModelAttribute ImageInDTO dto) {
        return ResponseEntity.ok(service.createOrUpdate(dto));
    }

    @GetMapping("/all")
    @Override
    public ResponseEntity<List<ImageOutDTO>> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        throw new FileNotFoundException("Not implemented!");
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<ImageOutDTO> getById(@PathVariable Integer id) {
        throw new FileNotFoundException("Not implemented!");
    }

    @GetMapping(value = "/{id}/download", produces = MediaType.IMAGE_JPEG_VALUE)
    public Resource download(@PathVariable Integer id) {
        return new ByteArrayResource(service.getImageContentById(id));
    }
}
