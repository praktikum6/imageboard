package org.svzh.praktikum.imageboard.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.svzh.praktikum.imageboard.db.ent.BanEntity;
import org.svzh.praktikum.imageboard.db.repo.BanRepository;
import org.svzh.praktikum.imageboard.services.generic.BanService;
import org.svzh.praktikum.imageboard.services.web.BanWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.BanInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.BanOutDTO;

@RestController
@RequestMapping("/ban")
@RequiredArgsConstructor
@Slf4j
public class BanController extends AbstractController<BanEntity, BanInDTO, BanOutDTO, BanRepository, BanService, BanWebService> {
}
