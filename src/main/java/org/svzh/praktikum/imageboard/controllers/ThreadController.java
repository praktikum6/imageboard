package org.svzh.praktikum.imageboard.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.svzh.praktikum.imageboard.db.ent.ThreadEntity;
import org.svzh.praktikum.imageboard.db.repo.ThreadRepository;
import org.svzh.praktikum.imageboard.services.generic.ThreadService;
import org.svzh.praktikum.imageboard.services.web.ThreadWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.ThreadInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.ThreadOutDTO;

@RestController
@RequestMapping("/thread")
@RequiredArgsConstructor
@Slf4j
public class ThreadController extends AbstractController<ThreadEntity, ThreadInDTO, ThreadOutDTO, ThreadRepository, ThreadService, ThreadWebService> {
}
