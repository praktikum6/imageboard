package org.svzh.praktikum.imageboard.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.svzh.praktikum.imageboard.db.ent.AbstractEntity;
import org.svzh.praktikum.imageboard.services.generic.AbstractService;
import org.svzh.praktikum.imageboard.services.web.AbstractWebService;
import org.svzh.praktikum.imageboard.utils.dto.in.AbstractInDTO;
import org.svzh.praktikum.imageboard.utils.dto.out.AbstractOutDTO;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Slf4j
public abstract class AbstractController<E extends AbstractEntity,
        I extends AbstractInDTO,
        O extends AbstractOutDTO,
        R extends JpaRepository<E, Integer>,
        S extends AbstractService<E, I, O, R>,
        W extends AbstractWebService<E, I, O, R, S>> {

    protected W service;

    @Autowired
    private void setService(W service) {
        this.service = service;
    }

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    @GetMapping("/all")
    public ResponseEntity<List<O>> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        return ResponseEntity.ok(service.getAll(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<O> getById(@PathVariable Integer id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @RequestMapping(method = {PUT, POST})
    public ResponseEntity<O> createOrUpdatePost(@RequestBody I dto) {
        return ResponseEntity.ok(service.createOrUpdate(dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> delete(@PathVariable Integer id) {
        service.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
