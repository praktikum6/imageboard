package org.svzh.praktikum.imageboard.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Excludes class or method from JaCoCo test coverage report
 */
@Documented
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface GeneratedOrConfigOrUtils {
}
