package org.svzh.praktikum.imageboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.svzh.praktikum.imageboard.annotations.GeneratedOrConfigOrUtils;

@SpringBootApplication
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
@EnableScheduling
public class ImageboardApplication {

    // TODO: 02.09.2021 podam вместо faker
    // TODO: 07.09.2021 базовый уй

    @GeneratedOrConfigOrUtils
    public static void main(String[] args) {
        SpringApplication.run(ImageboardApplication.class, args);
    }

}